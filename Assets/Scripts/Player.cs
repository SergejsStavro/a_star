using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class Player : MonoBehaviour
{
	[SerializeField] new Transform transform = default;

	bool shouldStop = false;
	bool isMoving = false;
	Coroutine movementCoroutine;

	public Transform Transform { get => transform; }
	public bool IsMoving { get => isMoving; }

	public event Action MovementCompleted = () => { };

	public void MoveAlongPath(List<Vector3> path)
	{
		if (movementCoroutine == null)
		{
			if (path.Count > 0)
				movementCoroutine = StartCoroutine(Move(path, 5, HandleMovementCompleted));
		}
	}

	public void Stop()
	{
		shouldStop = true;
	}

	IEnumerator Move(List<Vector3> path, float speed = 5f, Action onCompleted = null)
	{
		shouldStop = false;
		isMoving = true;
		Vector3 delta = Vector3.zero;
		for (int i = 0; i < path.Count && !shouldStop; i++)
		{
			var target = path[i];

			while ((transform.position.x != target.x || transform.position.y != target.y))
			{
				delta.x = target.x - transform.position.x;
				delta.y = target.y - transform.position.y;

				var distance = speed * Time.deltaTime;
				if (delta.sqrMagnitude < distance * distance)
					transform.position = target;
				else
					transform.position += delta.normalized * distance;

				yield return null;
			}
		}

		isMoving = false;
		if (onCompleted != null)
			onCompleted();
	}

	void HandleMovementCompleted()
	{
		movementCoroutine = null;
		MovementCompleted();
	}
}