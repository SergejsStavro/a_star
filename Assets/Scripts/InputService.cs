using System;
using UnityEngine;

public class InputService : MonoBehaviour
{
	public event Action<Vector3> MouseDown = (_) => { };

	void Update()
	{
		if (Input.GetMouseButtonDown(0))
			MouseDown(Input.mousePosition);
	}
}
