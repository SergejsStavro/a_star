using System.Collections.Generic;
using UnityEngine;

class TileGraph
{
	readonly int width;
	readonly int height;
	readonly List<Vector3Int> neighbourOffsets = new List<Vector3Int>
	{
		Vector3Int.right,
		Vector3Int.down,
		Vector3Int.left,
		Vector3Int.up,
	};

	HashSet<Vector3Int> nodes = new HashSet<Vector3Int>();
	Dictionary<Vector3Int, int> costs = new Dictionary<Vector3Int, int>();

	public TileGraph(int width, int height)
	{
		this.width = width;
		this.height = height;
	}

	public bool Contains(Vector3Int coordinates)
	{
		return
			0 <= coordinates.x && coordinates.x < width &&
			0 <= coordinates.y && coordinates.y < height;
	}

	public void AddPassable(Vector3Int coordinates, int cost)
	{
		costs.Add(coordinates, cost);
		nodes.Add(coordinates);
	}

	public bool IsPassable(Vector3Int coordinates)
	{
		return nodes.Contains(coordinates);
	}

	public int GetCost(Vector3Int coordinates)
	{
		int cost = 0;
		return costs.TryGetValue(coordinates, out cost) ? cost : -1;
	}

	public IEnumerable<Vector3Int> Neighbours(Vector3Int origin)
	{
		Vector3Int target = Vector3Int.zero;
		foreach (var offset in neighbourOffsets)
		{
			target.x = origin.x + offset.x;
			target.y = origin.y + offset.y;
			if (IsPassable(target))
				yield return target;
		}
	}
}