﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Main : MonoBehaviour
{
	[SerializeField] new Camera camera = default;
	[SerializeField] InputService inputService = default;
	[SerializeField] Tilemap tilemap = default;
	[SerializeField] Player player = default;

	AStar aStar = new AStar();
	TileGraph tileGraph = default;
	Stack<Vector3Int> targetStack = new Stack<Vector3Int>();

	void Awake()
	{
		inputService.MouseDown += HandleMouseDown;
		player.MovementCompleted += HandleMovementCompleted;

		tileGraph = BuildGraph(tilemap);
	}

	void HandleMouseDown(Vector3 mousePosition)
	{
		Vector3 mousePositionWorld = camera.ScreenToWorldPoint(mousePosition);

		MovePlayerToTargetTile(tilemap.WorldToCell(mousePositionWorld));
	}

	void HandleMovementCompleted()
	{
		if (targetStack.Count > 0)
		{
			player.MoveAlongPath(GetPlayerPath(targetStack.Pop()));
			targetStack.Clear();
		}
	}

	TileGraph BuildGraph(Tilemap tilemap)
	{
		var bounds = tilemap.cellBounds;
		var tileGraph = new TileGraph(bounds.size.x, bounds.size.y);

		Vector3Int pos = Vector3Int.zero;
		for (int yi = 0; yi < bounds.size.y; yi++)
		{
			for (int xi = 0; xi < bounds.size.x; xi++)
			{
				pos.x = xi;
				pos.y = yi;
				var colType = tilemap.GetColliderType(pos);

				if (colType == Tile.ColliderType.None)
					tileGraph.AddPassable(pos, 1);
			}
		}
		return tileGraph;
	}

	void MovePlayerToTargetTile(Vector3Int target)
	{
		if (tileGraph.IsPassable(target))
		{
			if (player.IsMoving)
			{
				targetStack.Push(target);
				player.Stop();
			}
			else
				player.MoveAlongPath(GetPlayerPath(target));
		}
	}

	List<Vector3> GetPlayerPath(Vector3Int target)
	{
		var origin = tilemap.WorldToCell(player.Transform.position);

		var tilemapPath = aStar.CalculatePath(tileGraph, origin, target);

		var worldPath = new List<Vector3>(tilemapPath.Count);
		for (int i = 0; i < tilemapPath.Count; i++)
			worldPath.Add(tilemap.CellToWorld(tilemapPath[i]));

		Vector3 offset = new Vector3(0.5f, 0.5f, 0);
		for (int i = 1; i < worldPath.Count; i++)
			Debug.DrawLine(worldPath[i - 1] + offset, worldPath[i] + offset, Color.magenta, 5);

		return worldPath;
	}
}
