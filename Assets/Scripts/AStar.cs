using System;
using System.Collections.Generic;
using UnityEngine;

class AStar
{
	public List<Vector3Int> CalculatePath(TileGraph tileGraph, Vector3Int origin, Vector3Int target)
	{
		return ExtractPath(PerformSearch(tileGraph, origin, target, ManhattanDistance), target);
	}

	Dictionary<Vector3Int, Vector3Int> PerformSearch(TileGraph tileGraph, Vector3Int start, Vector3Int goal, Func<Vector3Int, Vector3Int, int> heuristic)
	{
		HashSet<Vector3Int> openSet = new HashSet<Vector3Int>() { start };
		Dictionary<Vector3Int, Vector3Int> cameFrom = new Dictionary<Vector3Int, Vector3Int>();

		Dictionary<Vector3Int, int> gScore = new Dictionary<Vector3Int, int>() { { start, 0 } };
		Dictionary<Vector3Int, int> fScore = new Dictionary<Vector3Int, int>() { { start, heuristic(start, goal) } };

		Vector3Int current = Vector3Int.zero;
		int currentFScore = int.MaxValue;
		while (openSet.Count > 0)
		{
			current = Vector3Int.zero;
			currentFScore = int.MaxValue;

			foreach (var item in openSet)
			{
				int itemFScore;
				if (!fScore.TryGetValue(item, out itemFScore))
					itemFScore = int.MaxValue;

				if (itemFScore < currentFScore)
				{
					current = item;
					currentFScore = itemFScore;
				}
			}

			if (current == goal)
				return cameFrom;

			openSet.Remove(current);

			foreach (var neighbour in tileGraph.Neighbours(current))
			{
				int neighbourCost = tileGraph.GetCost(neighbour);
				int tentativeGScore = gScore[current] + neighbourCost;

				int neighbourGScore = 0;
				if (!gScore.TryGetValue(neighbour, out neighbourGScore))
					neighbourGScore = int.MaxValue;

				if (tentativeGScore < neighbourGScore)
				{
					cameFrom[neighbour] = current;
					gScore[neighbour] = tentativeGScore;
					fScore[neighbour] = gScore[neighbour] + heuristic(neighbour, goal);

					if (!openSet.Contains(neighbour))
						openSet.Add(neighbour);
				}
			}
		}

		return new Dictionary<Vector3Int, Vector3Int>();
	}

	List<Vector3Int> ExtractPath(Dictionary<Vector3Int, Vector3Int> paths, Vector3Int origin)
	{
		Stack<Vector3Int> stack = new Stack<Vector3Int>();

		if (paths?.Count > 0)
		{
			stack.Push(origin);
			Vector3Int current = origin;
			Vector3Int next;
			while (paths.TryGetValue(current, out next))
			{
				current = next;
				stack.Push(next);
			}
		}

		return new List<Vector3Int>(stack);
	}

	int ManhattanDistance(Vector3Int a, Vector3Int b)
	{
		return Mathf.Abs(a.x - b.x) + Mathf.Abs(a.y - b.y);
	}
}